import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Alert, AlertTitle } from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    "& > * + *": {
      marginTop: theme.spacing(2),
    },
    position: "relative",
    top: "-30px",
  },
  setBackgroundColor: {
    color: "rgb(58 17 14)",
    backgroundColor: "rgb(239 179 172)",
  },
}));

export default function DescriptionAlerts({ hasError }) {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {hasError ? (
        <Alert severity="error" className={classes.setBackgroundColor}>
          <AlertTitle>Error</AlertTitle>
          <strong>Số điện thoại</strong> hoặc <strong>mật khẩu</strong> không
          đúng!
        </Alert>
      ) : null}
    </div>
  );
}
