import React, { useEffect, useRef, useState } from "react";
import {
  CButton,
  CCard,
  CCardBody,
  CCol,
  CContainer,
  CForm,
  CInput,
  CInputGroup,
  CInputGroupPrepend,
  CInputGroupText,
  CRow,
} from "@coreui/react";
import CIcon from "@coreui/icons-react";
import { useCookies } from "react-cookie";
import DescriptionAlerts from "./alertLogin";

const Login = (props) => {

  const [phoneNumber, setPhoneNumber] = useState("");
  const [password, setPassword] = useState("");
  const [isCorrectPassWord, setIsCorrectPassWord] = useState(false);
  const [hasError, setHasError] = useState(null);
  const clientId = useRef(null);
  const [cookies, setCookie] = useCookies([
    "clientId",
    "password",
    "user",
    "userId",
  ]);

  useEffect(() => {
    clientId.current = "AB-" + Math.random().toString(16).substr(2, 8);
  }, []);
  useEffect(() => {
    if (
      cookies.password &&
      cookies.user &&
      cookies.clientId &&
      cookies.userId
    ) {
      props.history.replace("/dashboard");
    }
  }, [cookies, props.history]);

  /* useEffect(() => {
    if (isCorrectPassWord) {
      props.history.push("/dashboard");
    }
  }, [isCorrectPassWord, props.history]); */

  const checkLogin = async () => {
    const dataBody = {
      phone: phoneNumber,
      app_key: "854cbdacaca9d5216d1685546740661b",
      client_id: clientId.current,
      password: password,
      type: "farm",
      name: "ambio",
    };

    const res = await fetch("https://a.hongphuc.net/user/login", {
      method: "post",
      body: JSON.stringify(dataBody),
    });
    const data = await res.json();

    if (data.status !== "success") {
      setIsCorrectPassWord(false);
      setHasError(true);
    } else {
      const dataGetUserInfo = {
        access_token: data.access_token,
        app_key: "854cbdacaca9d5216d1685546740661b",
        client_id: clientId.current,
        type: "farm",
        name: "ambio",
      };

      const res = await fetch("https://a.hongphuc.net/user/info", {
        method: "post",
        body: JSON.stringify(dataGetUserInfo),
      });

      const dataUserInfor = await res.json();
      setIsCorrectPassWord(true);
      if (dataUserInfor.status === "success") {
        setCookie("password", data.access_token, {
          path: "/",
          expires: new Date(3000, 4, 7),
        });
        setCookie("user", data.mqtt_user, {
          path: "/",
          expires: new Date(3000, 4, 7),
        });
        setCookie("full_name", dataUserInfor.full_name, {
          path: "/",
          expires: new Date(3000, 4, 7),
        });
        setCookie("phone", dataUserInfor.dialing_code + dataUserInfor.phone, {
          path: "/",
          expires: new Date(3000, 4, 7),
        });
        setCookie("clientId", clientId.current, {
          path: "/",
          expires: new Date(3000, 4, 7),
        });
        setCookie("userId", dataUserInfor.user_id.toString(), {
          path: "/",
          expires: new Date(3000, 4, 7),
        });
      } else {
        console.log("error get user infor");
      }
    }
  };

  const handleEnterInput = (e) => {
    if (e.key === "Enter") {
      if (!password || !phoneNumber) return;

      checkLogin();
    }
  };

  const handleButton = () => {
    if (!password || !phoneNumber) return;

    checkLogin();
  };

  return (
    <div
      className="c-app c-default-layout flex-row align-items-center"
      style={{ minHeight: "80vh" }}
    >
      <CContainer>
        <DescriptionAlerts hasError={hasError} />
        <CRow className="justify-content-center">
          <CCol md="6">
            <CCard className="p-4">
              <CCardBody>
                <CForm>
                  <h1>Đăng Nhập</h1>
                  <p className="text-muted"></p>
                  <div className="wrap-input">
                    <CInputGroup>
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-user" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="text"
                        value={phoneNumber}
                        onChange={(e) => {
                          setPhoneNumber(e.target.value);
                        }}
                        onKeyPress={handleEnterInput}
                        placeholder="Nhập số điện thoại"
                        autoComplete="username"
                      />
                    </CInputGroup>
                  </div>
                  <div className="wrap-input">
                    <CInputGroup>
                      <CInputGroupPrepend>
                        <CInputGroupText>
                          <CIcon name="cil-lock-locked" />
                        </CInputGroupText>
                      </CInputGroupPrepend>
                      <CInput
                        type="password"
                        placeholder="Nhập mật khẩu"
                        autoComplete="current-password"
                        value={password}
                        onChange={(e) => {
                          setPassword(e.target.value);
                        }}
                        onKeyPress={handleEnterInput}
                      />
                    </CInputGroup>
                  </div>

                  <CRow>
                    <CCol xs="6">
                      <CButton
                        onClick={handleButton}
                        className="px-4"
                        style={{ backgroundColor: "#47AA12", color: "white" }}
                      >
                        Tiếp Tục
                      </CButton>
                    </CCol>
                  </CRow>
                </CForm>
              </CCardBody>
            </CCard>
          </CCol>
        </CRow>
      </CContainer>
    </div>
  );
};

export default Login;
