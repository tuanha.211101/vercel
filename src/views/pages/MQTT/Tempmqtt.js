import React, { useEffect, useState } from "react";
import mqtt from "mqtt";
import { useSelector } from "react-redux";
function Tempmqtt(props) {
  const [client, setClient] = useState(null);
  const [connectStatus, setConnectStatus] = useState(false);
  const { user, password, clientId, userId } = useSelector((state) => state.login);
  useEffect(() => {
    setClient(
      mqtt.connect("ws://test.mosquitto.org:8081/mqtt", {
        clientId: clientId,
        password: password,
        username: user,
      }) 
    );
  }, [clientId, password, user]);

  useEffect(() => {

    if (client) {

      client.on("connect", () => {
        console.log('connect');
        setConnectStatus(true);
      });


      client.on("message", (topic, message) => {
        console.log(message.toString());
      });

      client.subscribe(`proc_app/${userId}/farm/ambio/${clientId}/#`, (error) => {
        if (!error) {
          client.publish(`app_proc/${userId}/farm/ambio/${clientId}/home_list`, "1"); 
        }
      });

      /* client.subscribe(`tele/${userId}/2/#`, (error) => {
        if (!error){
        }
      }) */
    }

  }, [client, userId, clientId]);

  return <div>{connectStatus ? <div>CONNECT</div> : null}</div>;
}

export default Tempmqtt;
