import React from "react";
import { CCardBody, CCardHeader, CCard } from "@coreui/react";
import CIcon from "@coreui/icons-react";
function CardMaintain(props) {
  return (
    <CCard>
      <CCardHeader style={{height: '144px'}}>
          <h2 className='text-center mt-3' style={{color: '#f9b115'}}>Bảo Trì</h2>
          <p className='text-center'> (Thay đổi chế độ ăn ở phần cài đặt để tiếp tục cho ăn)</p>
      </CCardHeader>
      <CCardBody style={{ padding: 0 }}>
        <div className="card-group__poperty">
          <div>
            <i className="fas fa-battery-three-quarters" style={{width:'15px', fontSize: '15px'}}></i> Trạng thái
          </div>
          <div>Đang chạy</div>
        </div>
        <div className="card-group__poperty">
          <div>
            <CIcon size={"custom-size"} style={{width: '18px'}} name={"cil-speedometer"} />Tốc độ cho ăn
          </div>
          <div>5.8kg/giờ</div>
        </div>
        <div className="card-group__poperty">
          <div>
            {" "}
            <i className="far fa-clock"></i>Hết thức ăn
          </div>
          <div>14</div>
        </div>
        <div className="card-group__poperty">
          <div>
            <i className="fas fa-bowling-ball"> </i>Đã cho ăn trong cữ
          </div>
          <div>27kg</div>
        </div>
        <div className="card-group__poperty">
          <div>
            {" "}
            <i className="far fa-calendar-check"></i> Đã cho ăn hôm nay
          </div>
          <div>43kh</div>
        </div>
      </CCardBody>
    </CCard>
  );
}

export default CardMaintain;
