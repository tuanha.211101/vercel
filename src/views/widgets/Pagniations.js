import { CPagination } from "@coreui/react";
import React, { useState } from "react";
function Pagniations(props) {
  const [currentPage, setActivePage] = useState(2);

  return (
    <div className={"mt-2"}>
        <h3>Trang Trại</h3>
      <CPagination
        activePage={currentPage}
        pages={5}
        onActivePageChange={(i) => setActivePage(i)}
      ></CPagination>
    </div>
  );
}

export default Pagniations;
