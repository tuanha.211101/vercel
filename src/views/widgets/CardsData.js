import React from "react";
import { CRow, CCol } from "@coreui/react";
import CardData from "./CardData";
import { useSelector } from "react-redux";

const CardsData = () => {
  const dataDevices = useSelector((state) => state.dataDevices);
  const devices = useSelector((state) => state.devices);
  // render
  return (
    <CRow>
      {devices.data[0] ? (
        dataDevices ? (
          dataDevices.map((item, index) => {
            if (devices.data[index]) {
              return (
                <CCol sm="6" lg="3" key={index}>
                  <CardData
                    data={item}
                    name={devices.data[index]["device_name"]}
                  />
                </CCol>
              );
            } else {
              return null;
            }
          })
        ) : null
      ) : (
        <div
          style={{
            display: "flex",
            alignItems: "center",
            justifyContent: "center",
            width: '100%',
            height: '50vh'
          }}
          
        >
          <h3 style={{ color: "#6765659e" }}>Không có thiết bị nào</h3>
        </div>
      )}
    </CRow>
  );
};

export default CardsData;
