import React, { useContext, useEffect, useRef, useState } from "react";
import { CCardBody, CCardHeader, CCard } from "@coreui/react";

import useWindowDimensions from "./useWindowDimensions";
import { context } from "src/containers/TheLayout";

const STRING_UNDEFINED = "Chưa xác định";

function CardData({ data, name }) {
  const { publishHasFeeded } = useContext(context);
  const { LWT, cf, fa, feed, we, hasFeeded } = data;
  const [trangThai, setTrangThai] = useState(null);
  const [canNang, setCanNang] = useState(null);
  const [tocDoChoAn, setTocDoChoAn] = useState(null);
  const [duKienHetThucAn, setDuKienHetThucAn] = useState(null);
  const [daChoAnTrongCu, setDaChoAnTrongCu] = useState(null);
  const [countDown, setCountDown] = useState("00:00");
  const [loadding, setLoadding] = useState(false);
  const idInterval = useRef(null);

  const { width } = useWindowDimensions();

  function startTimer(duration) {
    var timer = duration,
      minutes,
      seconds;

    function startInterval() {
      minutes = parseInt(timer / 60, 10);
      seconds = parseInt(timer % 60, 10);

      if (width < 1352 && width > 575) {
        seconds = seconds < 10 ? "0" + seconds : seconds;
        minutes = minutes < 10 ? "0" + minutes : minutes;

        setCountDown(minutes + ":" + seconds);
      } else {
        seconds = seconds < 10 ? "0" + seconds : seconds;
        /* minutes = minutes < 10 ? "0" + minutes : minutes; */

        if (timer < 60) {
          setCountDown(seconds + " giây");
        } else {
          setCountDown(minutes + " phút " + seconds + " giây");
        }
      }

      if (--timer < 0) {
        setLoadding(true);
      } else {
        setLoadding(false);
      }
    }

    idInterval.current = setInterval(startInterval, 1000);
  }

  useEffect(() => {
    if (feed && cf) {
      publishHasFeeded(data.id); // publish lấy thức ăn đã cho hôm nay

      let currentTime = new Date().getTime();

      currentTime = currentTime / 1000; //  chuyển sang giây

      if (feed.feed === 0) {
        startTimer(cf.st - (currentTime - feed.time) - 3);
      } else if (feed.feed === 1) {
        startTimer(cf.rt - (currentTime - feed.time));
      } else {
        setCountDown("");
      }
    }

    return () => {
      clearInterval(idInterval.current);
    };
  }, [feed, cf]); // mỗi lần feed.feed thay đổi thì pub lấy thức ăn và đồng hồ chạy lại

  useEffect(() => {
    if (feed) {
      let result =
        feed.feed === 0
          ? "Đang nghỉ"
          : feed.feed === 1
          ? "Đang chạy"
          : "Đang tắt";
      setTrangThai(result);
    }
  }, [setTrangThai, feed]); // mổi lần feed.feed thay đổi thì trạng thái thay đổi

  useEffect(() => {
    if (feed && LWT === "Online" && we) {
      let convertObjArr = Object.keys(data);

      let weight =
        convertObjArr.findIndex((item) => item === "feed") >
        convertObjArr.findIndex((item) => item === "we")
          ? feed.we
          : we.we;
      setCanNang(Math.round(weight / 100) / 10); // chuyển sang kg và làm tròn 1 chũ số thập phân
    }

    if (feed && cf) {
      let convertObjArr = Object.keys(data);

      let speedBySecond =
        convertObjArr.findIndex((item) => item === "feed") >
        convertObjArr.findIndex((item) => item === "fa")
          ? feed.fa
          : fa.fa;

      let result =
        ((3600 - cf.rt) * cf.rt * speedBySecond) / (cf.rt + cf.st + 10) / 1000;
      result = parseFloat(result.toFixed(1));

      setTocDoChoAn(result);
    } else {
      setTocDoChoAn(STRING_UNDEFINED);
    }
  }, [feed, fa, cf, trangThai, LWT, we]); // set cân nặng và tốc độ cho ăn

  useEffect(() => {
    if (
      canNang &&
      canNang >= 0 &&
      tocDoChoAn &&
      tocDoChoAn !== STRING_UNDEFINED &&
      tocDoChoAn >= 0
    ) {
      let time = new Date().getTime();
      time = (canNang / tocDoChoAn) * 3600000 + time; // chuyển sang thời gian thực milisecond
      let date = new Date(time);
      let result = date
        .toString()
        .split(" ")[4]
        .split(":")
        .slice(0, 2)
        .join(":");
      setDuKienHetThucAn(result);
    } else {
      setDuKienHetThucAn(STRING_UNDEFINED); // tính toán dự kiến thức ăn
    }
  }, [canNang, tocDoChoAn, trangThai]);

  useEffect(() => {
    if (feed) {
      setDaChoAnTrongCu(Math.round(feed.fe / 100) / 10);
    } else {
      setDaChoAnTrongCu(0);
    }
  }, [feed]); // tính toán đã cho ăn

  if (LWT && cf && feed && fa) {
    return (
      <CCard>
        <h4
          style={{ textAlign: "center", position: "relative", top: "10px" }}
        >{`${name}`}</h4>

        {LWT === "Online" ? (
          <CCardHeader style={{ height: "144px" }}>
            <img
              className="card-img__head"
              width={130}
              height={100}
              src={"./img/containerFull.png"}
              alt=""
            />
            <img
              className="card-img__head"
              style={{
                clipPath: `inset(0 0 ${
                  canNang < 0 ? 0 : (canNang * 100) / 40
                }% 0 )`,
              }}
              width={130}
              height={100}
              src={"./img/containerZero.png"}
              alt=""
            />
            <h4 className="kilogram">{canNang} kg</h4>
          </CCardHeader>
        ) : cf.fm === "mt" ? (
          <CCardHeader style={{ height: "144px" }}>
            <h2 className="text-center mt-3" style={{ color: "#f9b115" }}>
              Bảo Trì
            </h2>
            <p className="text-center">
              {" "}
              (Thay đổi chế độ ăn ở phần cài đặt để tiếp tục cho ăn)
            </p>
          </CCardHeader>
        ) : (
          <CCardHeader style={{ height: "144px" }}>
            <h2 className="text-center mt-3" style={{ color: "#e55353" }}>
              Ngoại tuyến
            </h2>
            <p className="text-center">
              {" "}
              (Kiểm tra kết nối mạng hoặc nguồn của thiết bị)
            </p>
          </CCardHeader>
        )}

        <CCardBody style={{ padding: 0 }}>
          <div className="card-group__poperty">
            <div>
              <img src="./img/trangThai.svg" alt="" width={20} />
              <p>Trạng thái</p>
            </div>
            <div
              className={
                LWT !== "Online"
                  ? null
                  : trangThai === "Đang tắt"
                  ? "trang-thai__tat"
                  : trangThai === "Đang chạy"
                  ? "trang-thai__chay"
                  : null
              }
            >
              {LWT === "Online" ? (
                !loadding ? (
                  trangThai !== "Đang tắt" ? (
                    trangThai + ` (${countDown})`
                  ) : (
                    trangThai
                  )
                ) : (
                  <i className="fas fa-spinner fa-spin"></i>
                )
              ) : (
                STRING_UNDEFINED
              )}
            </div>
          </div>
          <div className="card-group__poperty">
            <div>
              <img src="./img/tocDoChoAn.svg" alt="" width={20} />
              <p>Tốc độ cho ăn</p>
            </div>
            <div>
              {LWT !== "Online"
                ? STRING_UNDEFINED
                : tocDoChoAn === STRING_UNDEFINED
                ? STRING_UNDEFINED
                : `${tocDoChoAn} kg/giờ`}
            </div>
          </div>
          <div className="card-group__poperty">
            <div>
              {" "}
              <img src="./img/duKienKetThuc.svg" alt="" width={20} />
              <p>Hết thức ăn</p>
            </div>
            <div>{LWT === "Online" ? duKienHetThucAn : STRING_UNDEFINED}</div>
          </div>
          <div className="card-group__poperty">
            <div>
              <img src="./img/daChoAnTrongCu.svg" alt="" width={20} />
              <p>Đã cho ăn trong cữ</p>
            </div>
            <div>
              {LWT === "Online" ? `${daChoAnTrongCu} kg` : STRING_UNDEFINED}
            </div>
          </div>
          <div className="card-group__poperty">
            <div>
              {" "}
              <img src="./img/daChoAnHomNay.svg" alt="" width={20} />
              <p>Đã cho ăn hôm nay</p>
            </div>
            <div>
              {hasFeeded
                ? `${Math.round(hasFeeded / 100) / 10} kg`
                : STRING_UNDEFINED}
            </div>
          </div>
        </CCardBody>
      </CCard>
    );
  } else
    return (
      <CCard>
        <h4
          style={{ textAlign: "center", position: "relative", top: "10px" }}
        >{`${name}`}</h4>
        {
          <CCardHeader style={{ height: "144px" }}>
            <h2 className="text-center mt-3" style={{ color: "#e55353" }}>
              Ngoại tuyến
            </h2>
            <p className="text-center">
              {" "}
              (Kiểm tra kết nối mạng hoặc nguồn của thiết bị)
            </p>
          </CCardHeader>
        }

        <CCardBody style={{ padding: 0 }}>
          <div className="card-group__poperty">
            <div>
              <img src="./img/trangThai.svg" alt="" width={20} />
              <p>Trạng thái</p>
            </div>
            <div
              className={
                LWT !== "Online"
                  ? null
                  : trangThai === "Đang tắt"
                  ? "trang-thai__tat"
                  : trangThai === "Đang chạy"
                  ? "trang-thai__chay"
                  : null
              }
            >
              {LWT === "Online"
                ? trangThai !== "Đang tắt"
                  ? trangThai + ` (${countDown})`
                  : trangThai
                : STRING_UNDEFINED}
            </div>
          </div>
          <div className="card-group__poperty">
            <div>
              <img src="./img/tocDoChoAn.svg" alt="" width={20} />
              <p>Tốc độ cho ăn</p>
            </div>
            <div>
              {LWT !== "Online"
                ? STRING_UNDEFINED
                : tocDoChoAn === STRING_UNDEFINED
                ? STRING_UNDEFINED
                : `${tocDoChoAn} kg/giờ`}
            </div>
          </div>
          <div className="card-group__poperty">
            <div>
              {" "}
              <img src="./img/duKienKetThuc.svg" alt="" width={20} />
              <p>Hết thức ăn</p>
            </div>
            <div>{LWT === "Online" ? duKienHetThucAn : STRING_UNDEFINED}</div>
          </div>
          <div className="card-group__poperty">
            <div>
              <img src="./img/daChoAnTrongCu.svg" alt="" width={20} />
              <p>Đã cho ăn trong cữ</p>
            </div>
            <div>
              {LWT === "Online" ? `${daChoAnTrongCu} kg` : STRING_UNDEFINED}
            </div>
          </div>
          <div className="card-group__poperty">
            <div>
              {" "}
              <img src="./img/daChoAnHomNay.svg" alt="" width={20} />
              <p>Đã cho ăn hôm nay</p>
            </div>
            <div>
              {hasFeeded
                ? `${Math.round(hasFeeded / 100) / 10} kg`
                : STRING_UNDEFINED}
            </div>
          </div>
        </CCardBody>
      </CCard>
    );
}

export default CardData;
