import { CAlert, CCol, CRow } from "@coreui/react";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";
import CardsData from "../widgets/CardsData.js";

const Dashboard = () => {
  const dataDevices = useSelector((state) => state.dataDevices);
  const [dangTat, setDangTat] = useState(0);
  const [dangHetThucAn, setDangHetThucAn] = useState(0);
  const [dangChoAn, setDangChoAn] = useState(0);
  const [daChoAn, setDaChoAn] = useState(0);
  const [ngoaiTuyen, setNgoaiTuyen] = useState(0);

  useEffect(() => {
    let tempDangTat = 0;
    let tempDangHetThucAn = 0;
    let tempDangChoAn = 0;
    let tempDaChoAn = 0;
    let tempNgoaiTuyen = 0;
    dataDevices.forEach((item) => {
      if (item.feed && item.LWT) {
        if (item.LWT === "Online") {
          if (item.feed.feed === 3) tempDangTat++;

          if (item.feed.we < 200) tempDangHetThucAn++;

          if (item.feed.feed === 1 || item.feed.feed === 0 ) tempDangChoAn++;

          if (item.hasFeeded) tempDaChoAn += item.hasFeeded;
        } else {
          tempNgoaiTuyen++;
          if (item.hasFeeded) tempDaChoAn += item.hasFeeded;
        }
      }
    });

    setDangTat(tempDangTat);
    setDangHetThucAn(tempDangHetThucAn);
    setNgoaiTuyen(tempNgoaiTuyen);
    setDangChoAn(tempDangChoAn);
    setDaChoAn(Math.round(tempDaChoAn / 1000));
  }, [dataDevices]);

  return (
    <>
      <CAlert
        style={{
          backgroundColor: "white",
          fontSize: "16px",
          padding: "25px 5px",
        }}
      >
        <CRow>
          <CCol
            lg={2}
            sm={6}
            style={{ color: "rgb(123 123 123 / 71%)" }}
            className="text-center"
          >
            {`Đang tắt: ${dangTat} máy`}
          </CCol>

          <CCol
            lg={3}
            sm={6}
            style={{ color: "rgb(236,148,44)" }}
            className="text-center"
          >
            {`Hết thức ăn: ${dangHetThucAn} máy`}
          </CCol>

          <CCol
            lg={2}
            sm={6}
            style={{ color: "rgb(71,170,18)" }}
            className="text-center"
          >
            {`Đang cho ăn: ${dangChoAn} máy`}
          </CCol>

          <CCol
            lg={3}
            sm={6}
            style={{ color: "rgb(45,90,171)" }}
            className="text-center"
          >
            {`Đã cho ăn hôm nay: ${daChoAn} kg`}
          </CCol>

          <CCol
            lg={2}
            sm={12}
            style={{ color: "rgb(207,43,42)", padding: "0 10px 0 0" }}
            className="text-center"
          >
            {`Ngoại tuyến: ${ngoaiTuyen} máy`}
          </CCol>
        </CRow>
      </CAlert>
      <CardsData />
    </>
  );
};

export default Dashboard;
