const initialState = [];

const reduceDataDevices = (state = initialState, { type, payload }) => {
  switch (type) {
    case "UPDATEDATADEVICE":
      let { headTopic, infor, topic } = payload;
      if (headTopic === "procApp_devices") {
        return [
          ...state,
          ...infor.map((item) => ({
            id: item["device_id"],
          })),
        ];
      } else if (headTopic === "tele" || headTopic === "stat") {
        let index = state.findIndex((item) => topic.includes(item.id));
        let topicSplit = topic.split("/");
        let tailTopic = topicSplit[topicSplit.length - 1];
        if (tailTopic !== "LWT") {
          infor = JSON.parse(infor);
        }
        if (index !== -1) {
          return [
            ...state.slice(0, index),
            { ...state[index], [tailTopic]: infor },
            ...state.slice(index + 1),
          ];
        } else {
          return state;
        }
      } else if (headTopic === "stats_feed") {
        infor = JSON.parse(infor);
        let index = state.findIndex((item) => item.id === infor["device_id"]);
        return [
          ...state.slice(0, index),
          { ...state[index], hasFeeded: infor.data[0].feed },
          ...state.slice(index + 1),
        ];
      }

    case "REMOVEDATADEVICE":
      return [];
    default:
      return state;
  }
};

export default reduceDataDevices;
