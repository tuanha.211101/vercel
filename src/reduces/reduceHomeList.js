const initialState = {
    data: null,
    shares: null
  };
  
  const reduceHomeList = (state = initialState, action) => {
    switch (action.type) {
      case "PUSHDATAHOMELIST":
        return {
          ...state,
          data: action.payload
        };

        case "PUSHDATASHAREHOMELIST": 
          return {
            ...state,
            shares: action.payload
          }
  
      default:
        return state;
    }
  };
  
  export default reduceHomeList;
  