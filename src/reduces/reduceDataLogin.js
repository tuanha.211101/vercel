const initialState = {
  user: "",
  password: "",
  clientId: "",
  userId: "",
  full_name: "",
  phone: "",
};

const reduceDataLogin = (state = initialState, action) => {
  switch (action.type) {
    case "PUSHDATALOGIN":
      const {
        user,
        password,
        clientId,
        userId,
        full_name,
        phone,
      } = action.payload;
      return {
        user,
        password,
        clientId,
        userId,
        full_name,
        phone,
      };

    default:
      return state;
  }
};

export default reduceDataLogin;
