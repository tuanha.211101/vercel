import { combineReducers } from 'redux'
import changeState from './reduceChangeState'
import reduceDataLogin from './reduceDataLogin'
import reduceDataDevices from './reduceDataDevices'
import reduceDevices from './reduceDevices'
import reduceHomeList from './reduceHomeList'
const ReduceAll = combineReducers({
    login: reduceDataLogin,
    changeState: changeState,
    HomeList: reduceHomeList,
    devices: reduceDevices,
    dataDevices: reduceDataDevices
})

const rootReducer = (state, action) => {
    if (action.type === 'USER_LOGOUT') {
      state = undefined
    }
  
    return ReduceAll(state, action)
  }

export default rootReducer