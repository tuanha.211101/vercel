const initialState = {
    data: []
  };
  
  const reduceDevices = (state = initialState, action) => {
    switch (action.type) {
      case "PUSHDEVICES":
        return {
          data: action.payload
        };
  
      default:
        return state;
    }
  };
  
  export default reduceDevices;
  