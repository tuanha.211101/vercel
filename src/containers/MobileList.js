import React, { useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import StarBorder from "@material-ui/icons/StarBorder";
import ScreenShareOutlinedIcon from "@material-ui/icons/ScreenShareOutlined";
import ArrowBackIosOutlinedIcon from "@material-ui/icons/ArrowBackIosOutlined";
import { context } from "./TheLayout";
import { useDispatch, useSelector } from "react-redux";
import removeDataDevice from "src/actions/Device/removeDataDevice";
import StarIcon from "@material-ui/icons/Star";

const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    backgroundColor: theme.palette.background.paper,
    height: "100%",
    paddingTop: "20px",
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  primary: {
    textAlign: "center",
    fontWeight: 600,
    fontSize: "18px",
  },
  iconStar: {
    color: "#f9b115",
    transform: "scale(1.2,1.2)",
    transition: "transform 2s",
  },
}));

export default function MobileList({
  setMobileList,
  mobileList,
  homeList,
  setNameSelect,
}) {
  const classes = useStyles();
  const [openMy, setOpenMy] = React.useState(true);
  const [openShare, setOpenShare] = React.useState(true);
  const [homeIdSelect, setHomeIdSelect] = React.useState(null);
  const [userIdSelect, setUserIdSelect] = React.useState(null);
  const { getHomeData, subDevices, unsubDevices } = useContext(context);
  const disPatch = useDispatch();
  const { userId } = useSelector((state) => state.login);

  const selectHome = (home) => {
    if (home["home_id"])
      home = { id: home["home_id"], user_id: home["user_id"] };
    disPatch(removeDataDevice());
    setHomeIdSelect(home.id);
    setUserIdSelect(home["user_id"]);
    localStorage.setItem(
      `${userId}_homeSelect`,
      JSON.stringify({
        homeIdLocalStorage: home.id,
        userIdLocalStorage: home["user_id"],
      })
    );
    unsubDevices(homeIdSelect, userIdSelect);
    getHomeData(home);
    subDevices(home);
  };

  useEffect(() => {
    if (homeList.data) {
      const dataLocal = JSON.parse(
        localStorage.getItem(`${userId}_homeSelect`)
      );
      if (dataLocal) {
        const { homeIdLocalStorage, userIdLocalStorage } = dataLocal;
        setHomeIdSelect(homeIdLocalStorage);
        setUserIdSelect(userIdLocalStorage);
      } else {
        const homeId = homeList.data[0].id;
        localStorage.setItem(
          `${userId}_homeSelect`,
          JSON.stringify({
            homeIdLocalStorage: homeId,
            userIdLocalStorage: userId,
          })
        );
        setHomeIdSelect(homeId);
        setUserIdSelect(userId);
      }
    }
  }, [homeList.data]);

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
    >
      <ListItem button onClick={() => setMobileList(!mobileList)}>
        <ListItemIcon>
          <ArrowBackIosOutlinedIcon />
        </ListItemIcon>
        <ListItemText
          classes={{ primary: classes.primary }}
          primary="Chọn Trang Trại"
        />
      </ListItem>

      <ListItem button onClick={() => setOpenMy(!openMy)}>
        <ListItemIcon>
          <InboxIcon />
        </ListItemIcon>
        <ListItemText primary="Trang trại của tôi" />
        {openMy ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <Collapse in={openMy} timeout="auto" unmountOnExit>
        {homeList.data
          ? homeList.data.map((item, index) => (
              <ListItem
                button
                className={classes.nested}
                key={index}
                onClick={(e) => {
                  selectHome(item);
                  setNameSelect(item["home_name"]);
                  setMobileList(!mobileList);
                }}
              >
                <ListItemIcon>
                  {homeIdSelect === item.id ? (
                    <StarIcon className={classes.iconStar} />
                  ) : (
                    <StarBorder />
                  )}
                </ListItemIcon>
                <ListItemText primary={item["home_name"]} />
              </ListItem>
            ))
          : null}
      </Collapse>

      <ListItem button onClick={() => setOpenShare(!openShare)}>
        <ListItemIcon>
          <ScreenShareOutlinedIcon />
        </ListItemIcon>
        <ListItemText primary="Được chia sẻ" />
        {openShare ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <Collapse in={openShare} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <StarBorder />
            </ListItemIcon>
            <ListItemText primary="Trại 1 - Khu 1" />
          </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <StarBorder />
            </ListItemIcon>
            <ListItemText primary="Trại 2 - Khu 2" />
          </ListItem>
          <ListItem button className={classes.nested}>
            <ListItemIcon>
              <StarBorder />
            </ListItemIcon>
            <ListItemText primary="Trại 3 - Khu 3" />
          </ListItem>
        </List>
      </Collapse>
    </List>
  );
}
