import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CHeader,
  CToggler,
  CHeaderNav,
} from '@coreui/react'

// routes config

import {
  TheHeaderDropdown,
 
} from './index'

import CustomizedMenus from './CustomizedMenus'



const TheHeader = () => {
  const dispatch = useDispatch()
  const sidebarShow = useSelector(state => state.changeState.sidebarShow)

  const toggleSidebar = () => {
    const val = [true, 'responsive'].includes(sidebarShow) ? false : 'responsive'
    dispatch({ type: 'set', sidebarShow: val })
  }

  const toggleSidebarMobile = () => {
    const val = [false, 'responsive'].includes(sidebarShow) ? true : 'responsive'
    dispatch({ type: 'set', sidebarShow: val })
  }

  return (
    <CHeader withSubheader>
      
      

      <CustomizedMenus />

      <CHeaderNav className="ml-auto px-3">
        
        <TheHeaderDropdown />
      </CHeaderNav>


    </CHeader>
  )
}

export default TheHeader
