import React from 'react'
import { useSelector, useDispatch } from 'react-redux'
import {
  CCreateElement,
  CSidebar,
  CSidebarBrand,
  CSidebarNav,
  CSidebarNavDivider,
  CSidebarNavTitle,
  CSidebarMinimizer,
  CSidebarNavDropdown,
  CSidebarNavItem,
} from '@coreui/react'


// sidebar nav config
import navigation from './_nav'

const TheSidebar = () => {
  const dispatch = useDispatch()
  const show = useSelector(state => state.changeState.sidebarShow)
  return (
    <CSidebar
      show={show}
      onShowChange={(val) => { dispatch({ type: 'set', sidebarShow: val }) }}
    >
      <CSidebarBrand className="d-md-down-none" to="/">
        {/* <CIcon
          className="c-sidebar-brand-full"
          name="logo-negative"
          height={35}
        /> */}
        <div className="c-sidebar-brand-full">
          <img src="./img/logo.png" width={50} alt="" />
          <h2 style={{alignSelf: 'flex-end', color: '#77AA42'}}>Ambio</h2>
        </div>
        <div className="c-sidebar-brand-minimized">
          <img src="./img/logo.png" width={50} alt="" />
        </div>
      </CSidebarBrand>

      <CSidebarNav >

        <CCreateElement
          items={navigation}
          components={{
            CSidebarNavDivider,
            CSidebarNavDropdown,
            CSidebarNavItem,
            CSidebarNavTitle
          }}
        />
      </CSidebarNav>

      <CSidebarMinimizer className="c-d-md-down-none" />
    </CSidebar>
  )
}

export default React.memo(TheSidebar)
