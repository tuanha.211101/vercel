import React, { useContext, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Collapse from "@material-ui/core/Collapse";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExpandLess from "@material-ui/icons/ExpandLess";
import ExpandMore from "@material-ui/icons/ExpandMore";
import StarBorder from "@material-ui/icons/StarBorder";
import ScreenShareOutlinedIcon from "@material-ui/icons/ScreenShareOutlined";
import StarIcon from "@material-ui/icons/Star";
import { context } from "./TheLayout";
import { useDispatch, useSelector } from "react-redux";
import removeDataDevice from "src/actions/Device/removeDataDevice";
const useStyles = makeStyles((theme) => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper,
  },
  nested: {
    paddingLeft: theme.spacing(4),
  },
  iconStar: {
    color: "#f9b115",
    transform: "scale(1.2,1.2)",
    transition: "transform 2s",
  },
}));

export default function NestedList({ homeList, setNameSelect, closeNestList }) {
  const classes = useStyles();
  const [openMy, setOpenMy] = React.useState(true);
  const [openShare, setOpenShare] = React.useState(true);
  const [homeIdSelect, setHomeIdSelect] = React.useState(null);

  const [userIdSelect, setUserIdSelect] = React.useState(null);

  const { getHomeData, subDevices, unsubDevices } = useContext(context);
  const disPatch = useDispatch();

  const { shares: homeShares } = useSelector((state) => state.HomeList);

  const { userId } = useSelector((state) => state.login);

  const selectHome = (home) => {
    if (home["home_id"])
      home = { id: home["home_id"], user_id: home["user_id"] };
    disPatch(removeDataDevice());
    setHomeIdSelect(home.id);
    setUserIdSelect(home["user_id"]);
    localStorage.setItem(
      `${userId}_homeSelect`,
      JSON.stringify({
        homeIdLocalStorage: home.id,
        userIdLocalStorage: home["user_id"],
      })
    );
    unsubDevices(homeIdSelect, userIdSelect);
    getHomeData(home);
    subDevices(home);
  };

  useEffect(() => {
    if (homeList.data) {
      const dataLocal = JSON.parse(
        localStorage.getItem(`${userId}_homeSelect`)
      );

      if (dataLocal) {
        const { homeIdLocalStorage, userIdLocalStorage } = dataLocal;
        subDevices({ id: homeIdLocalStorage, user_id: userIdLocalStorage });
        getHomeData({ id: homeIdLocalStorage, user_id: userIdLocalStorage });
        setHomeIdSelect(homeIdLocalStorage);
        setUserIdSelect(userIdLocalStorage);
        let index = homeList.data.findIndex(
          (item) => item.id === homeIdLocalStorage
        );
        if (index === -1) {
          if (homeShares) {
            index = homeShares.findIndex(
              (item) => item["home_id"] === homeIdLocalStorage
            );
            console.log(homeShares, index, dataLocal);

            setNameSelect(homeShares[index]["home_name"]);
          }
        } else {
          setNameSelect(homeList.data[index]["home_name"]);
        }
      } else {
        const homeId = homeList.data[0].id;
        localStorage.setItem(
          `${userId}_homeSelect`,
          JSON.stringify({
            homeIdLocalStorage: homeId,
            userIdLocalStorage: userId,
          })
        );
        subDevices({ id: homeId, user_id: userId });
        getHomeData({ id: homeId, user_id: userId });
        setHomeIdSelect(homeId);
        setUserIdSelect(userId);
        setNameSelect(homeList.data[0]["home_name"]);
      }
    }
  }, [homeList.data]);

  return (
    <List
      component="nav"
      aria-labelledby="nested-list-subheader"
      className={classes.root}
    >
      <ListItem button onClick={() => setOpenMy(!openMy)}>
        <ListItemIcon>
          <InboxIcon />
        </ListItemIcon>
        <ListItemText primary="Trang trại của tôi" />
        {openMy ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <Collapse in={openMy} timeout="auto" unmountOnExit>
        {homeList.data
          ? homeList.data.map((item, index) => (
              <ListItem
                button
                className={classes.nested}
                key={index}
                onClick={(e) => {
                  selectHome(item);
                  setNameSelect(item["home_name"]);
                  closeNestList(e);
                }}
              >
                <ListItemIcon>
                  {homeIdSelect === item.id ? (
                    <StarIcon className={classes.iconStar} />
                  ) : (
                    <StarBorder />
                  )}
                </ListItemIcon>
                <ListItemText primary={item["home_name"]} />
              </ListItem>
            ))
          : null}
      </Collapse>

      <ListItem button onClick={() => setOpenShare(!openShare)}>
        <ListItemIcon>
          <ScreenShareOutlinedIcon />
        </ListItemIcon>
        <ListItemText primary="Được chia sẻ" />
        {openShare ? <ExpandLess /> : <ExpandMore />}
      </ListItem>

      <Collapse in={openShare} timeout="auto" unmountOnExit>
        <List component="div" disablePadding>
          {homeShares
            ? homeShares.map((item, index) => (
                <ListItem
                  button
                  className={classes.nested}
                  key={index}
                  onClick={(e) => {
                    selectHome(item);
                    setNameSelect(item["home_name"]);
                    closeNestList(e);
                  }}
                >
                  <ListItemIcon>
                    {homeIdSelect === item["home_id"] ? (
                      <StarIcon className={classes.iconStar} />
                    ) : (
                      <StarBorder />
                    )}
                  </ListItemIcon>
                  <ListItemText primary={item["home_name"]} />
                </ListItem>
              ))
            : null}
        </List>
      </Collapse>
    </List>
  );
}
