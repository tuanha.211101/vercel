import React from "react";
import {
  CDropdown,
  CDropdownItem,
  CDropdownMenu,
  CDropdownToggle,
  CImg,
} from "@coreui/react";
import { useHistory } from "react-router";
import ExitToAppIcon from "@material-ui/icons/ExitToApp";
import { useCookies } from "react-cookie";
import { useDispatch, useSelector } from "react-redux";
import { makeStyles } from "@material-ui/core";
import FaceIcon from '@material-ui/icons/Face';
import PhoneOutlinedIcon from '@material-ui/icons/PhoneOutlined';
const TheHeaderDropdown = () => {
  const history = useHistory();
  const [cookies, setCookie, removeCookie] = useCookies(["as"]);
  const {phone, full_name } = useSelector(
    (state) => state.login
  );
  const dispatch = useDispatch();
  function logOut() {
    removeCookie("clientId");
    removeCookie("password");
    removeCookie("user");
    removeCookie("userId");
    removeCookie("full_name");
    removeCookie("phone");
    dispatch({ type: "USER_LOGOUT" });
    history.push("./login");
  }

  const useStyles = makeStyles({
    root: {
      marginRight: "5px",
      marginLeft: "-1px",
    },
  });
  const classes = useStyles();

  return (
    <CDropdown inNav className="c-header-nav-items mx-2" direction="down">
      <CDropdownToggle className="c-header-nav-link" caret={false}>
        <div className="c-avatar">
          <CImg
            src={"./avatars/avt.png"}
            className="c-avatar-img"
            alt="admin@bootstrapmaster.com"
          />
        </div>
      </CDropdownToggle>
      <CDropdownMenu className="pt-0" placement="bottom-end">
        <CDropdownItem header tag="div" color="light" className="text-center">
          <strong>Account</strong>
        </CDropdownItem>
        <CDropdownItem >
          <FaceIcon fontSize="small" className={classes.root} />
          <span className='ml-2'>{full_name ? full_name : null}</span>
          
        </CDropdownItem>
        <CDropdownItem >
          <PhoneOutlinedIcon fontSize="small" className={classes.root} />
          <span className='ml-1'>{phone ? phone : null}</span>
        </CDropdownItem>

        <CDropdownItem divider />
        <CDropdownItem onClick={logOut}>
          <ExitToAppIcon fontSize="small" className={classes.root} />
          <span className='ml-1'>Đăng xuất</span>
        </CDropdownItem>
      </CDropdownMenu>
    </CDropdown>
  );
};

export default TheHeaderDropdown;
