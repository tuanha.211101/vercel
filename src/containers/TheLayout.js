import React, { createContext, useEffect, useRef, useState } from "react";
import { TheContent, TheFooter, TheHeader } from "./index";
import mqtt from "mqtt";
import { useDispatch, useSelector } from "react-redux";
import pushDataHomeList from "src/actions/HomeList/pushDataHomeList.js";
import pushDevices from "src/actions/Devices/pushDevices.js";
import updateDataDevice from "src/actions/Device/updateDataDevice.js";
import { useCookies } from "react-cookie";
import pushDataLogin from "src/actions/Login/pushDataLogin";
import pushDataShareHomeList from "src/actions/HomeList/pushDataShareHomeList";
export const context = createContext();

const TheLayout = (props) => {
  const [client, setClient] = useState(null);

  const { user, password, clientId, userId } = useSelector(
    (state) => state.login
  );

  const dispatch = useDispatch();

  const [cookies, setCookie, removeCookie] = useCookies([
    "clientId",
    "password",
    "user",
    "userId",
  ]);

  /* lấy dữ liệu login từ cookies rồi dispatch lên redux, rồi get dữ liệu login từ redux về để đăng
   nhập mqtt */

  /*  kết nối client xong thì nhận các event và pub, sub các topic để lấy dữ liệu */

  /* các hàm:
  publishHasFeeded: gửi tín hiệu để get dữ liệu đã ăn trong hôm nay
  getHomeData: get data của một home nào đó
  unsubDevices: unsub các topic để lấy dữ liệu devices của một home 
  subDevices: ngược lại với thằng trên  */

  /* thứ tự topic lấy data:
    home_list
    devices
    tele
    stat
    stats_feed */

  const { history } = props;
  const currentPathname = useRef(null);
  const currentSearch = useRef(null);

  /* useEffect(() => {
    history.listen((newLocation, action) => {
      if (action === "PUSH") {
        if (
          newLocation.pathname !== currentPathname.current ||
          newLocation.search !== currentSearch.current
        ) {
          currentPathname.current = newLocation.pathname;
          currentSearch.current = newLocation.search;

          history.push({
            pathname: newLocation.pathname,
            search: newLocation.search,
          });
        }
      } else {
        history.go(1)
      }
    });
  }, []); */

  useEffect(() => {
    if (clientId) {
      setClient(
        mqtt.connect("ws://hongphuc.net:2013/mqtt", {
          clientId: clientId,
          password: password,
          username: user,
        })
      );
    }
  }, [clientId, password, user]);

  useEffect(() => {
    if (
      cookies.password &&
      cookies.user &&
      cookies.clientId &&
      cookies.userId
    ) {
      dispatch(
        pushDataLogin({
          password: cookies.password,
          user: cookies.user,
          clientId: cookies.clientId,
          userId: parseInt(cookies.userId),
          phone: cookies.phone,
          full_name: cookies.full_name,
        })
      );
    } else {
      props.history.push("./login");
    }
  }, [cookies, props.history, dispatch]);

  useEffect(() => {
    if (client) {
      client.on("disconnect", function (packet) {
        console.log("disconnect");
        console.log(packet);

        client.end();
        removeCookie("clientId");
        removeCookie("password");
        removeCookie("user");
        removeCookie("userId");
        dispatch({ type: "USER_LOGOUT" });
        props.history.push("./login");
      });

      client.on("close", function () {
        console.log("close");
      });

      client.on("error", function (error) {
        console.log("ERROR: ", error);
        client.end();
        removeCookie("clientId");
        removeCookie("password");
        removeCookie("user");
        removeCookie("userId");
        dispatch({ type: "USER_LOGOUT" });
        props.history.push("./login");
      });

      client.on("offline", function () {
        console.log("offline");
      });

      client.on("reconnect", function () {
        console.log("reconnect");
      });

      client.on("connect", () => {
        console.log("Connect");
      });

      client.on("message", (topic, message) => {
        if (topic.includes("home_list")) {
          let homeList = JSON.parse(message.toString());
          dispatch(pushDataShareHomeList(homeList.shares));
          dispatch(pushDataHomeList(homeList.data));
        }

        if (topic.includes("/devices")) {
          let devices = JSON.parse(message.toString()).devices;
          dispatch(pushDevices(devices));
          dispatch(
            updateDataDevice({
              headTopic: "procApp_devices",
              infor: devices,
              topic: topic,
            })
          );
        }

        if (topic.includes("tele/")) {
          dispatch(
            updateDataDevice({
              infor: message.toString(),
              headTopic: "tele",
              topic: topic,
            })
          );
        }

        if (topic.includes("stat/")) {
          dispatch(
            updateDataDevice({
              infor: message.toString(),
              topic: topic,
              headTopic: "stat",
            })
          );
        }
        if (topic.includes("stats_feed")) {
          dispatch(
            updateDataDevice({
              infor: message.toString(),
              topic: topic,
              headTopic: "stats_feed",
            })
          );
        }
      });

      client.subscribe(
        `proc_app/${userId}/farm/ambio/${clientId}/#`,
        (error) => {
          if (!error) {
            client.publish(
              `app_proc/${userId}/farm/ambio/${clientId}/home_list`,
              "1"
            );
          }
        }
      );
    }
  }, [client, userId, clientId, dispatch]);

  const subDevices = (home) => {
    client.subscribe(`tele/ab/${home["user_id"]}/${home.id}/#`, (error) => {
      if (!error) {
        client.subscribe(
          `stat/ab/${home["user_id"]}/${home.id}/#`,
          (error) => {}
        );
      }
    });
  };

  const unsubDevices = (homeId, userIdSelect) => {
    client.unsubscribe(`tele/ab/${userIdSelect}/${homeId}/#`, (error) => {
      if (!error) {
        client.unsubscribe(
          `stat/ab/${userIdSelect}/${homeId}/#`,
          (error) => {}
        );
      }
    });
  };

  const getHomeData = (home) => {
    client.publish(
      `app_proc/${userId}/farm/ambio/${clientId}/devices`,
      `{"home_id": "${home.id}" }`
    );
  };

  const publishHasFeeded = (idDevice) => {
    var today = new Date();

    var date =
      today.getFullYear() +
      "/" +
      (today.getMonth() + 1).toString().padStart(2, "0") +
      "/" +
      today.getDate().toString().padStart(2, 0);

    if (client) {
      client.publish(
        `app_proc/${userId}/farm/ambio/${clientId}/stats_feed`,
        `{ "device_id": "${idDevice}",
        "time_from":  "${date}",
        "time_end" : "${date}",
        "interval" : "day" }`
      );
    }
  };

  return (
    <context.Provider
      value={{ getHomeData, subDevices, unsubDevices, publishHasFeeded }}
    >
      <div className="c-app c-default-layout">
        <div className="c-wrapper">
          <TheHeader />
          <div className="c-body">
            <TheContent />
          </div>
          <TheFooter />
        </div>
      </div>
    </context.Provider>
  );
};

export default TheLayout;
