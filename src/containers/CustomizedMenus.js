import React, { useEffect } from "react";
import { makeStyles, withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";

import NestedList from "./NestList";
import MobileList from "./MobileList";
import { useSelector } from "react-redux";

const StyledMenu = withStyles({
  paper: {
    border: "1px solid #d3d4d5",
  },
})((props) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: "bottom",
      horizontal: "center",
    }}
    transformOrigin={{
      vertical: "top",
      horizontal: "center",
    }}
    {...props}
  />
));

const StyledMenuItem = withStyles(() => ({
  root: {
    padding: 0,
  },
}))(MenuItem);

const useStyles = makeStyles(() => ({
  root: {
    backgroundColor: "#47AA12",
    width: "232px",
    "&:hover": {
      backgroundColor: "#47AA12",
    },
  },
}));



export default function CustomizedMenus() {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [mobileList, setMobileList] = React.useState(false);
  const classWrap = useStyles();

  const homeList = useSelector((state) => state.HomeList);
  const [nameSelect, setNameSelect] = React.useState("Chọn Trang Trại");
  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
    setMobileList(!mobileList);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const closeNestList = () => {
    setAnchorEl(null);
  };

  return (
    <div className="ml-auto mt-2">
      <Button
        aria-controls="customized-menu"
        aria-haspopup="true"
        variant="contained"
        color="primary"
        onClick={handleClick}
        className={classWrap.root}
      >
        {nameSelect}
        <ExpandMoreIcon />
      </Button>

      <StyledMenu
        id="customized-menu"
        anchorEl={anchorEl}
        keepMounted
        open={Boolean(anchorEl)}
        onClose={handleClose}
        className="d-none d-sm-block"
      >
        <StyledMenuItem>
          <NestedList
            homeList={homeList}
            setNameSelect={setNameSelect}
            closeNestList={closeNestList}
          />
        </StyledMenuItem>
      </StyledMenu>

      <div
        className={mobileList ? "mobile-list d-sm-none" : "mobile-list d-none"}
      >
        <MobileList
          setMobileList={setMobileList}
          mobileList={mobileList}
          homeList={homeList}
          setNameSelect={setNameSelect}
        />
      </div>
    </div>
  );
}
