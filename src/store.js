import { createStore } from "redux";
import ReduceAll from "./reduces";

const store = createStore(
  ReduceAll,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
);
export default store;
