
const pushDataLogin = ({ user, password, clientId, userId, full_name, phone }) => {
  return {
    payload:{
      user,password,clientId, userId, full_name, phone
    },
    type: 'PUSHDATALOGIN'
  };
};

export default pushDataLogin
